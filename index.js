const express = require("express");
const mongoose =require("mongoose");
const app = express();
const port = 4000;


mongoose.connect("mongodb+srv://admin:admin123@cluster0.il0ermo.mongodb.net/bookingAPI?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

let db=mongoose.connection;

db.on('error',console.error.bind(console, "MongoDB Connection Error."))

db.once('open',()=> console.log("Connected to MongoDB."))


app.use(express.json());

const coursesRoutes = require("./routes/coursesRoutes");

app.use("/courses",coursesRoutes);

const userRoutes = require("./routes/userRoutes");

app.use("/users",userRoutes);

app.listen(port,()=>console.log(`Server is running at port ${port}`));