const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
//const Course = require("../models/Course");
const auth = require("../auth");

const{verify,verifyAdmin} = auth;
// router.get('/',(req,res)=>{
// 	// res.send("This route will get all course documents")
// 		Course.find({})
// 		.then(result => res.send(result))
// 		.catch(error => res.send(error))
// });
router.get("/",verify,verifyAdmin,courseControllers.getAllCourses);


router.post("/",verify,verifyAdmin,courseControllers.addCourse);
// router.post('/',(req,res)=>{
	
// 	// console.log(req.body);

// 	let newCourse = new Course({

// 				name: req.body.name,
// 				description:req.body.description,
// 				price: req.body.price

// 	})


// 		newCourse.save()
// 		.then(result=> res.send(result))
// 		.catch(error => res.send(error))
// });
router.get("/activeCourses",courseControllers.getActiveCourses);

router.get("/singleCourse/:courseId",courseControllers.getSingleCourse);

router.put("/updateCourse/:courseId",courseControllers.updateCourse);


router.delete("/archiveCourse/:courseId",verify,verifyAdmin,courseControllers.archiveCourse);

module.exports = router;
