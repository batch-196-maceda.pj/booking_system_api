const express = require("express");
const router = express.Router();


const userControllers = require("../controllers/userControllers");

//console.log(userControllers);

const auth = require("../auth");

const {verify} = auth;

router.post("/",userControllers.registerUser);
//const bcrypt= require("bcrypt");
//const User = require("../models/User");

// router.post("/",(req,res)=>{

// const hashedPw=bcrypt.hashSync(req.body.password,10);
// console.log(hashedPw);

// let newUser = new User({

// 				firstName: req.body.firstName,
// 				lastName:req.body.lastName,
// 				email: req.body.email,
// 				password:hashedPw,
// 				mobileNumber:req.body.mobileNumber

// 	});


// 		newUser.save()
// 		.then(result=> res.send(result))
// 		.catch(error => res.send(error))

	

// });

 router.get("/details",verify,userControllers.getUserDetails);
// // let _id = (User, objectId)

// // User.find({"_id":req.body.id}) mongodb and fin 0ne
// // User.findOne({"_id":req.body.id})
// 	User.findById(req.body.id) //mongoose
// 		.then(result => res.send(result))
// 		.catch(error => res.send(error))
// });

router.post('/login',userControllers.loginUser);

router.post("/usersEmail",userControllers.foundUserEmail);

//User enrollment

router.post('/enroll',verify,userControllers.enroll);

module.exports = router;