

const Course = require("../models/Course");

module.exports.getAllCourses=(req,res)=>{
	// res.send("This route will get all course documents")


		Course.find({})
		.then(result => res.send(result))
		.catch(error => res.send(error))
};

module.exports.addCourse =(req,res)=>{
	
	// console.log(req.body);

	let newCourse = new Course({

				name: req.body.name,
				description:req.body.description,
				price: req.body.price

	})


		newCourse.save()
		.then(result=> res.send(result))
		.catch(error => res.send(error))
};


module.exports.getActiveCourses = (req,res) =>{
	
	Course.find({isActive:true})
	.then(result=> res.send(result))
	.catch(error => res.send(error))

}

module.exports.getSingleCourse = (req,res)=>{


	//console.log(req.params)
//console.log(req.params.courseId)

	Course.findById(req.params.courseId)
	.then(result => res.send(result))
	.catch(error => res.send(error))

	
}

module.exports.updateCourse =(req,res)=>{

//console.log(req.params.courseId);


	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))	
	// console.log(req.body);

}

module.exports.archiveCourse = (req,res)=>{

	//console.log(req.params.courseId)
	let update={
		isActive:false
	}
	Course.findByIdAndUpdate(req.params.courseId,update,{new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}